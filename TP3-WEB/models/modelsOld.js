var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LocationSchema = new Schema({
    id: Number,
    nom: String,
    commentaire: String,
    dateCreation: String,
    latlng: String,
    type: String
}, {
    _id: false
});

var CollectionSchema = new Schema({
    id: Number,
    nom: String,
    locations:  [LocationSchema]
});


module.exports.LocationsModel = mongoose.model('Collections', CollectionSchema);
module.exports.PointsModel = mongoose.model('Points', LocationSchema);