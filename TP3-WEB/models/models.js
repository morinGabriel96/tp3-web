
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = require('mongoose').Schema.ObjectId;


var LieuSchema = new Schema({
    nom: {type: String},
    commentaire: {type: String},
    dateCreation: {type: Date, default: new Date(+new Date() - 4*60*60*1000)},
    latlng: [{
                lat : String, 
                lng: String
            }],
    type: {type: String}
});

var CollectionSchema = new Schema({
    nom: {type: String, required: true},
    locations:  [LieuSchema]
});

var UserSchema = new Schema({
    nom: {type: String, required: true, unique: true},
    email: {type: String, required: true},
    passw: {type: String, required: true},
    salt: String,
    collections: [CollectionSchema]
});



module.exports.User = mongoose.model('User', UserSchema);
module.exports.Collection = mongoose.model('Collection', CollectionSchema);
module.exports.Lieu = mongoose.model('Lieu', LieuSchema);
//module.exports = mongoose.model('User', UserSchema);
