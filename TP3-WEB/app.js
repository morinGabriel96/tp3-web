var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine','pug');
var mongoose = require('mongoose');
var uri = "mongodb://andgsk:qwerty123@clustertp3-shard-00-00-z2gje.mongodb.net:27017,clustertp3-shard-00-01-z2gje.mongodb.net:27017,clustertp3-shard-00-02-z2gje.mongodb.net:27017/test?ssl=true&replicaSet=ClusterTP3-shard-0&authSource=admin";
mongoose.connect(uri);
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var cookieParser = require('cookie-parser');

app.use(cookieParser());


/*
encryptionUtil = 
    encryptPassword: (password, salt) ->
        salt ?= bcrypt.genSaltSync()
        encryptedPassword = bcrypt.hashSync(password, salt)
        {salt, encryptedPassword}

    comparePassword: (password, salt, encryptedPasswordToCompareTo) ->
        {encryptedPassword} = @encryptPassword(password, salt)
        encryptedPassword == encryptedPasswordToCompareTo
*/


var ObjectId = require('mongoose').Schema.ObjectId;

var UserModel = require('./models/models').User;
var CollectionModel = require('./models/models').Collection;
var LieuModel = require('./models/models').Lieu;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.set('secret', 'tp3web');

var router = express.Router();
var routergeneral = express.Router();

var salt = bcrypt.genSaltSync(10);


router.post('/authenticate', function(req, res) {
    var username = req.body.nom;
    var passw = req.body.passw;

    if (username == null || username.trim() == "")
    {
        res.status(400);
        res.json("Le nom est obligatoire");
    }

    if (passw == null || passw.trim() == "")
    {
        res.status(400);
        res.json("Le mot de passe est obligatoire");
    }

    username = username.trim();
    passw = passw.trim();

    UserModel.findOne({
        nom: username
    }, function(err, userNoAuth) {
        if (userNoAuth != null) {
            var encryptedPassword = bcrypt.hashSync(passw, userNoAuth.salt);
            UserModel.findOne({
                nom: username,
                passw: encryptedPassword
            }, function(err, user) {
                if (err) {
                    console.log(err);
                } else {
                    if (!user) {
                        res.statusCode = 404;
                        res.json({
                            success: false,
                            message: 'Invalide.'
                        });
                    } else if (user) {

                        if (user.passw != encryptedPassword) {
                            res.json({
                                success: false,
                                message: 'Invalide.'
                            });
                        } else {
                            const payload = {
                                id: user._id
                            };
                            var token = jwt.sign(payload, app.get('secret'), {
                                expiresIn: 86400 // expire dans 24 heures
                            });

                            res.statusCode = 200;
                            // retour du token
                            res.json({
                                success: true,
                                message: 'Token valide',
                                token: token
                            });
                        }
                    }
                }

            });


        } 
        else 
        {

            res.statusCode = 200;
            res.json({
                    message: 'Utilisateur introuvable'
                });
        } 
    });
});


router.route('/membres')

    .post(function(req, res) {

        var user = new UserModel();
        var nom = req.body.nom;
        var email = req.body.email;
        var passw = req.body.passw;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        if (email == null || email.trim() == "")
        {
            res.status(400);
            res.json({"message": "Courriel est obligatoire"});
            return;
        }


        if (passw == null || passw.trim() == "")
        {
            res.status(400);
            res.json({"message": "Mot de passe est obligatoire"});
            return;
        }

        var user = new UserModel();
        user.nom = nom.trim();
        user.email = email.trim();
        user.salt = salt;
        user.collections = req.body.collections;

        var encryptedPassword = bcrypt.hashSync(passw.trim(), salt);
        user.passw = encryptedPassword;

        user.save(function(err) {
            if (err) {
                if(err.code = 11000)
                {
                    res.statusCode = 400;
                    res.json({
                        message: err//"Nom d'utilisateur est deja utilisé."
                    });
                }
                else 
                {
                    res.status(500);
                    console.log(err);
                }
            } else {
                delete user[passw];
                delete user[salt];

                res.statusCode = 201;
                res.json({
                    user: user
                });
            }


        });

    });
 
var tokenmiddleware = function(req,res,next){
    console.log(req.originalUrl);
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;
    if (token == null || token == "") {
        token = cookieParser.JSONCookie("token");
    }

    if (req.headers.authorization != null) {

        token = req.headers.authorization.split(' ')[1];

    }
    // decoder le token si il existe
    if (token) {
        // verification avec le secret
        jwt.verify(token, app.get('secret'), function(err, decoded) {
            if (err) {
                console.log(215);
                res.render('connexion', {title: 'Connexion', message: 'Connectez vous pour pouvoir accéder à cette page.'});
            } else {

                    console.log("token OK");
                // si le token est vrai on sauvegarde dans req pour les autres routes
                req.decoded = decoded;
                //next();
            next();
            }
        });
    } else {
            console.log(225);
           res.render('connexion', {title: 'Connexion', message: 'Connectez vous pour pouvoir accéder à cette page.'});
    }


};
/*router.use(function(req, res, next) {
    console.log(req.method, req.url);

    //On ramasse le token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (req.headers.authorization != null) {
        token = req.headers.authorization.split(' ')[1];

    }
    // decoder le token si il existe
    if (token) {
        // verification avec le secret
        jwt.verify(token, app.get('secret'), function(err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // si le token est vrai on sauvegarde dans req pour les autres routes
                req.decoded = decoded;
                //next();
            }
        });
    } else {
        // Sans token
        // retourner une erreure
        return res.status(403).send({ 
                success: false, 
                message: 'Pas de token.' 
            });
        }


        next();
    });
 */
router.get('/', function(req, res) {
    res.render('index', {title: 'TP3-WEB-CALLS'});
});

router.route('/membres')
    
    .get(tokenmiddleware,function(req, res) {
        UserModel.find({},["nom", "email", "collections"], function(err, users) {
            if (err) {
                console.log(err);
            } else {
                res.statusCode = 200;
                res.json(users);
            }

        });
    })/*.delete(function(req, res) {
        var id = req.decoded.id;

        UserModel.remove({
            _id: id
        }, function(err, user) {
            if (err)
            {
                res.status(500);
                console.log(err);
            } 
            else
            {
                res.status(204);
                res.json({
                    message: 'Votre compte a été supprimé avec succès'
                });
            } 
            
        });
    });*/
	.delete(function(req, res) {
        UserModel.remove({
            
        }, function(err, user) {
            if (err)
                console.log(err);

            res.json({
                message: 'Successfully deleted all'
            });
        });
    });


router.route('/membres/:user_id')

    .get(tokenmiddleware,function(req, res) {
        UserModel.findById(req.params.user_id,["nom", "email", "collections"], function(err, user) {
            if (err) {
                res.status(500);
                console.log(err);
            } else {
                res.status(200);
                res.json(user);
            }
        }); 
    });

    /*.put(function(req, res) {

        var user = new UserModel();
        var nom = req.body.nom;
        var email = req.body.email;
        var passw = req.body.passw;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        if (email == null || email.trim() == "")
        {
            res.status(400);
            res.json({"message": "Courriel est obligatoire"});
            return;
        }


        if (passw == null || passw.trim() == "")
        {
            res.status(400);
            res.json({"message": "Mot de passe est obligatoire"});
            return;
        }

        var id = req.decoded.id;
        var encryptedPassword = bcrypt.hashSync(passw.trim(), salt);

        var user = {
            nom: nom.trim(),
            email: email.trim(),
            passw: encryptedPassword,
            salt: salt
        };

        UserModel.findOneAndUpdate({_id: req.decoded.id}, {$set:user},function(err, userAjour){
            if(err){

                res.status(500);
                console.log(err);
            } else {

                delete userAjour[passw];
                delete userAjour[salt];
                res.status(200);
                res.json(userAjour);
            }

        });
    });*/

router.route('/lieux')
    .get(tokenmiddleware,function(req, res) {
        UserModel.find({}, function(err, arrUsers) {
            var arrCollections = [];
            if (err)
            {
                res.status(500);
                console.log(err);
            }
            else
            {
                for (var i = 0; i < arrUsers.length; i++)
                {
                    var collections = arrUsers[i].collections;
                    var dejaLa = false;
                    for (var j = 0; j < arrUsers.length; j++)
                    {
                        if (arrUsers[j] === collections || collections.length == 0)
                        {
                            dejaLa = true;
                        }
                    }
  
                    if (!dejaLa)
                    {
                        arrCollections.push(collections);
                    }
                }

                var arrLocations = [];
                for (var i = 0; i < arrCollections.length; i++)
                {                    
                    var uneCollectionArray = arrCollections[i];
                    var uneCollection = uneCollectionArray[0];
                    
                    var dejaLa = false;

                    if(uneCollection.locations != null)
                    {
                        for (var j = 0; j < uneCollection.locations.length; j++)
                        {
                            if (arrLocations[j] === uneCollection.locations[j])
                            {
                                dejaLa = true;
                            }

                            if (!dejaLa)
                            {
                                arrLocations.push(uneCollection.locations[j]);
                            }
                        }                       
                    }
                }
                res.statusCode = 200;
                res.json(arrLocations);
            }
        });
    });

router.route('/lieux/:collection_id')
    .post(tokenmiddleware,function(req, res) {
  
        var collection_id = req.params.collection_id;

        var nom = req.body.nom;
        var commentaire = req.body.commentaire;
        var latlng = req.body.latlng;
        var type = req.body.type;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        if (commentaire == null || commentaire.trim() == "")
        {
            res.status(400);
            res.json({"message": "Commentaire est obligatoire"});
            return;
        }


        if (type == null || type.trim() == "")
        {
            res.status(400);
            res.json({"message": "Le type de lieu est obligatoire"});
            return;
        }

        if (latlng == null || latlng.length < 1)
        {
            res.status(400);
            res.json({"message": "Au moins une coordonnée géocraphique nécessaire"});
            return;
        }

        var lieuAdd = {
            "_id": new mongoose.Types.ObjectId(),
            "nom": nom.trim(),
            "commentaire": commentaire.trim(),
            "latlng": latlng,
            "type": type.trim()
        };

        UserModel.find({
            _id: req.decoded.id
        }, function(err, user) {
            if (err) {
                console.log(err);
                res.status(500);
            } else {
                if (user != null) {
                    if (user.collections == null)
                        user.collections = [];

                    UserModel.update(
                        { "_id": req.decoded.id, "collections._id": collection_id }, 
                        { $push: { "collections.$.locations": lieuAdd } }, function(err, lieu) {
                            if (err) {
                                console.log(err);
                                res.status(500);
                            } else {

                                res.statusCode = 201;
                                res.json({
                                    lieu: lieuAdd
                                }); 
                            }
                        });


                } else {
                    res.statusCode = 404;
                    res.json("Utilisateur n'est pas connecté.")
                }

            }

        });

    });

router.route('/lieux/:lieu_id')
    .delete(tokenmiddleware,function(req, res) {
        var id = req.params.lieu_id;
        UserModel.findOne({'_id': req.decoded.id}, function (err, result) {
            if (err) {
                console.log(err);
                res.status(500);
            } else {

                for (var i = 0; i < result.collections.length; i++) {
                    for (var j = 0; j < result.collections[i].locations.length; j++)
                    {
                        if (result.collections[i].locations[j].id == id)
                        {
                            result.collections[i].locations[j].remove();
                        }
                    }
                   
                }
                result.save();

                res.json({
                        message: 'Supprimé'
                });
            }

        });
    }).get(tokenmiddleware,function(req, res) {
        var id = req.params.lieu_id;
        UserModel.findOne({'_id': req.decoded.id}, function (err, result) {
            if (err) {
                console.log(err);
                res.status(500);
            } else {
                for (var i = 0; i < result.collections.length; i++) {
                    for (var j = 0; j < result.collections[i].locations.length; j++)
                    {
                        if (result.collections[i].locations[j].id == id)
                        {
                            res.json({
                                    "lieu":result.collections[i].locations[j]
                            });
                        }
                    }
                   
                }
            }

        });
            
    }).put(tokenmiddleware,function(req, res) {

        var user = new UserModel();
        var nom = req.body.nom;
        var commentaire = req.body.commentaire;
        var latlng = req.body.latlng;
        var type = req.body.type;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        if (commentaire == null)
        {
            res.status(400);
            res.json({"message": "Commentaire est obligatoire"});
            return;
        }


        if (type == null || type.trim() == "")
        {
            res.status(400);
            res.json({"message": "Le type est obligatoire"});
            return;
        }

        if (latlng == null || latlng.length < 1)
        {
            res.status(400);
            res.json({"message": "Au moins une coordonnée géocraphique est nécessaire"});
            return;
        }


        var id = req.params.lieu_id;

        nom = nom.trim();
        commentaire = commentaire.trim();
        type = type.trim();



        UserModel.findById(req.decoded.id, function (err, user) {
            if (err) {
                console.log(err);
                res.status(500);
            } else {
                for (var i = 0; i < user.collections.length; i++) {
                    var locations = user.collections[i].locations;
                    for (var j = 0; j < locations.length; j++) {
                        if (locations[j]._id == id)
                        {
                            locations[j].nom = nom;
                            locations[j].commentaire = commentaire;
                            locations[j].latlng = latlng;
                            locations[j].type = type;

                            user.save();

                            res.statusCode = 201;
                            res.json(locations[j]);
                        }
                    }
                }
            }

              // or if you want to change more then one field -
              //=> var t = data.sub1.id(_id1).sub2.id(_id2);
              //=> t.field = req.body.something;

        });
        
    });

router.route('/collections')
    .get(tokenmiddleware,function(req, res) {
        UserModel.find({"_id": req.decoded.id}, function(err, arrUsers) {
            if (err) {
                console.log(err);
                res.status(500);
            } else {
                var arrCollections = [];
                for (var i = 0; i < arrUsers.length; i++) {
                    var collections = arrUsers[i].collections;
                    var dejaLa = false;
                    for (var j = 0; j < arrCollections.length; j++) {
                        if (arrCollections[j] === collections || collections.length == 0) {
                            dejaLa = true;
                        }
                    }


                    if (!dejaLa) {
                        arrCollections.push(collections);
                        /*
                        for (var i = 0; i < collections.length; i++)
                        {
                            arrCollections.push(collections[i]);
                        }*/
                    }


                }
                //arrCollections = arrUsers.distinct("User.Collections");
                res.statusCode = 200;
                res.json(arrCollections);
            }
        });
    })
    .post(tokenmiddleware,function(req, res) {

        

        UserModel.find({
            _id: req.decoded.id
        }, function(err, user) {
           var nom = req.body.nom;
        var locations = req.body.locations;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        if (locations == null)
        {
            locations = [];
        }


        var collAdd = {
            "nom" : nom.trim(),
            "locations" : locations
        }; if (err) {
                console.log(err);
                res.status(500);
            } else {
                if (user != null) {
                    if (user.collections == null)
                        user.collections = [];

                    UserModel.update(
                        { _id: req.decoded.id }, 
                        { $push: { collections: collAdd } }, function(err, user) {
                            if (err) {
                                console.log(err);
                            }}
                    );

                    UserModel.findOne({'collections.nom': nom}, function (err, result) {
                        
                        if (err) {
                            console.log(err);
                            res.status(500);
                        } else {
                            for (var i = 0; i < result.collections.length; i++) {
                                if (result.collections[i].nom == nom)
                                {
                                    
                                    res.statusCode = 201;
                                    res.json({
                                            "collection":result.collections[i]
                                    });
                                }
                            }
                        }
                    });

                    res.statusCode = 201;

                } else {
                    res.statusCode = 404;
                }
            }
        });
    });

router.route('/Collections/:collection_id')
    .get(tokenmiddleware,function(req, res) {
        var id = req.params.collection_id;

        UserModel.findOne({'collections._id': id}, function (err, result) {
            
            if (err) {
                console.log(err);
                res.status(500);
            } else {

                for (var i = 0; i < result.collections.length; i++) {
                    if (result.collections[i].id == id)
                    {
                        
                        res.json({
                                "collection":result.collections[i]
                        });
                    }
                }
            }


        });
            
        })
    .put(tokenmiddleware,function(req, res) {
        var id = req.params.collection_id;

        var nom = req.body.nom;
        //var locations = req.body.locations;

        if (nom == null || nom.trim() == "")
        {
            res.status(400);
            res.json({"message": "Nom est obligatoire"});
            return;
        }

        nom = nom.trim();

        UserModel.findOne({'collections._id': id}, function (err, result) {

            if (err) {
                console.log(err);
                res.status(500);
            } else {
                UserModel.findOneAndUpdate({ "collections._id" : id}, { "collections.$.nom" : nom }, function(err, doc){
                    
                    if (err) {
                        console.log(err);
                        res.status(500);
                    } else {
                        res.json({collection: doc});//collection?
                    }
                });
            }
        });
    })


    .delete(tokenmiddleware,function(req, res) {
        var id = req.params.collection_id;
        UserModel.findOne({'_id': req.decoded.id}, function (err, result) {

            if (err) {
                console.log(err);
                res.status(500);
            } else {

                console.log("SEARCHING FOR");
                for (var i = 0; i < result.collections.length; i++) {
                    if (result.collections[i].id == id)
                    {
                        console.log("FOUND AND DELETED");
                        result.collections[i].remove();
                    }
                }
                result.save();
                res.status(204);
                res.json({
                        message: 'Successfully deleted'
                });
            }

        });
    });


router.route('/collections/:collection_id/:user_id')
    .post(tokenmiddleware,function(req, res) {

        console.log("POST ACTION DÉBUT");
        // ICI
        var from_user_id = req.decoded.id;
        var to_user_id = req.params.user_id;
        var collection_id = req.params.collection_id;
        var collection;

        UserModel.findOne({'collections._id': collection_id}, function (err, result) {
            if (err) {
                console.log(err);
                console.log(867);
                res.status(500);
            } else {


                for (var i = 0; i < result.collections.length; i++) {
                    if (result.collections[i].id == collection_id)
                    {
                        collection = result.collections[i];
                    }
                }
            }
        });

        console.log("POST Utilisateur trouvé");
        UserModel.findOne({
            _id: to_user_id
        }, function(err, user) {
            
            if (err) {
                console.log(886);
                console.log(err);
                res.status(500);
            } else {

                if (user != null) {
                    if (user.collections == null)
                        user.collections = [];

                    var collectionExists = false;
                    var existingCollection;

                    
                    for (var i = 0; i < user.collections.length; i++)
                    {
                        //if (user.collections[i].nom == collection.nom)
                        if (user.collections[i].nom == "Import")
                        {
                            existingCollection = user.collections[i];
                            collectionExists = true;
                        }
                    }


                    
                    if (collectionExists && collection != null)
                    {
                    console.log("POST collection existe");
                        for (var i = 0; i < collection.locations.length; i++) {
                            var location = collection.locations[i];
                            location._id = new mongoose.Types.ObjectId();
                            UserModel.update(
                                { "_id": to_user_id, "collections._id": existingCollection._id }, 
                                { $push: { "collections.$.locations": location } }, function(err, user) {
                                if (err) {
                                    console.log(err);
                                    res.status(500);
                                } else {

                                    res.statusCode = 201;
                                    console.log("rep : " + collection);
                                    res.json({
                                        collection: collection
                                    }); 
                                }
                            });
                        }
                    }
                    else 
                    {
                    console.log("POST collection existe pas");
                        collection._id = new mongoose.Types.ObjectId();
                        collection.nom = "Import";
                        for (var i = 0; i < collection.locations.length; i++) {
                            collection.locations[i]._id = new mongoose.Types.ObjectId();
                        }

                        UserModel.update(
                            { _id: to_user_id }, 
                            { $push: { collections: collection } }, function(err, user) {
                                if (err) {
                                    console.log(err);
                                    res.status(500);
                                } else {
                                    
                                    res.statusCode = 201;

                                    console.log("rep : " + collection);
                                    res.json({
                                        collection: collection
                                    }); 
                                }
                        });
                    }
                } else {
                    res.statusCode = 404;
                }
                //arrCollections = arrUsers.distinct("User.Collections");

            }

        });

    });


router.route('/lieux/:lieu_id/:user_id')
    .post(tokenmiddleware, function(req, res) {
        var from_user_id = req.decoded.id;
        var to_user_id = req.params.user_id;
        var lieu_id = req.params.lieu_id;
        var lieu = {}, collection = {};

        UserModel.findOne({'collections.locations._id': lieu_id}, function (err, result) {

            if (err) {
                console.log(err);
                res.status(500);
            } else {

                for (var i = 0; i < result.collections.length; i++) {
                    for (var j = 0; j < result.collections[i].locations.length; j++)
                    {
                        if (result.collections[i].locations[j].id == lieu_id)
                        {
                            lieu = result.collections[i].locations[j];
                        }
                    }
                   
                }
            }
        });

        UserModel.findOne({
            _id: to_user_id
        }, function(err, user) {
            
            if (err) {
                console.log(err);
                res.status(500);
            } else {

                if (user != null) {
                    if (user.collections == null)
                        user.collections = [];

                    var collectionExists = false;
                    var existingCollection;

                    
                    for (var i = 0; i < user.collections.length; i++)
                    {
                        if (user.collections[i].nom == "Import")
                        {
                            existingCollection = user.collections[i];
                            collectionExists = true;
                        }
                    }

                    lieu._id = new mongoose.Types.ObjectId();

                    if (collectionExists)
                    {

                        UserModel.update(
                            { "_id": to_user_id, "collections._id": existingCollection._id }, 
                            { $push: { "collections.$.locations": lieu } }, function(err, user) {
                            if (err) {
                                console.log(err);
                                res.status(500);
                            }
                            else {

                                res.statusCode = 201;
                                res.json({
                                    lieu: lieu
                                }); 
                            }
                        });

                        


                    }
                    else 
                    {

                        collection._id = new mongoose.Types.ObjectId();
                        collection.nom = "Import";
                        collection.locations = [];
                        collection.locations.push(lieu);

                        UserModel.update(
                            { _id: to_user_id }, 
                            { $push: { collections: collection } }, function(err, user) {
                                if (err) {
                                    console.log(err);
                                    res.status(500);
                                }
                                else {

                                    res.statusCode = 201;
                                    res.json({
                                        lieu: lieu
                                    }); 
                                }
                        });
                    }
                } else {
                    res.statusCode = 404;
                }

            }

        });

    });
    routergeneral.route('/')
    
    .get(tokenmiddleware,function(req, res) {



        res.render('index', {title: 'TP3-WEB-General'});
    });
    routergeneral.route('/connexion')
    
    .get(function(req, res) {
        res.render('connexion', {title: 'Connexion'});
    });

    routergeneral.route('/inscription')
    
    .get(function(req, res) {
        res.render('inscription', {title: 'Inscription'});
    });

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);
app.use('/', routergeneral);
module.exports = app;

// START THE SERVER
// =============================================================================



/*var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();


var mongoose = require('mongoose');
var uri = "mongodb://andgsk:qwerty123@tp3-web-shard-00-00-3qwlx.mongodb.net:27017,tp3-web-shard-00-01-3qwlx.mongodb.net:27017,tp3-web-shard-00-02-3qwlx.mongodb.net:27017/test?ssl=true&replicaSet=TP3-WEB-shard-0&authSource=admin";

// Connexion à MongoDB avec Mongoose.
mongoose.connect(uri);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/membres', usersRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var port = process.env.PORT || 8080;

app.listen(port,function(){

  console.log('Serveur Node.js à l\'écoute sur 8080');
});

module.exports = app;
*/